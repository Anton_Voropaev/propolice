//
//  AVServerManager.m
//  ProPolice
//
//  Created by Anton Voropaev on 11.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import "AVServerManager.h"
#import <AFHTTPSessionManager.h>
#import <FEMMapping.h>
#import <FEMDeserializer.h>
#import "SectionModel.h"
#import "ArticleModel.h"

@interface AVServerManager ()
@property (strong, nonatomic) AFHTTPSessionManager *manager;
@property (strong, nonatomic) NSMutableArray *models;

@end

@implementation AVServerManager

static NSInteger const numberOfItems = 11;

+ (AVServerManager *)sharedManager {
    static AVServerManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[AVServerManager alloc] init];
    });
    return manager;
}

- (instancetype)init //with AFHTTPmanager base URL
{
    self = [super init];
    if (self) {
        
        self.models = [NSMutableArray array];
        NSURL *url = [NSURL URLWithString:@"https://tranquil-mountain-37185.herokuapp.com/"];
        self.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        
    }
    return self;
}


- (void)getDataFromServerWithSkip:(NSInteger)skip andMethodName:(NSString*)method andParams:(NSDictionary*)params onSuccses:(void(^)(NSArray*items))succses onFailure:(void(^)(NSError *error))failure {
    
    
        [self.manager GET:method parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"RESPONSE %@", responseObject);
            
            if ([method containsString:@"section"] && ![method containsString:@"article"]) {
                
                FEMMapping *mappig = [SectionModel defaultMapping];
                NSArray *models = [FEMDeserializer collectionFromRepresentation:responseObject mapping:mappig];
                succses(models);
                
            }else if ([method containsString:@"article"]) {
                
                FEMMapping *mapping = [ArticleModel defaultMapping];
                NSArray *models = [FEMDeserializer collectionFromRepresentation:responseObject mapping:mapping];
                succses(models);
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            error = [NSError errorWithDomain:@"Downloading failed" code:-1 userInfo:nil];
            failure(error);
        }];


}

- (void)findItemsWithParams:(NSDictionary *)dictionary onSuccses:(void(^)(NSArray*items))succses onFailure:(void(^)(NSError *error))failure {

    //?parts=searchText&extended=parts
    
    NSString *url = @"https://tranquil-mountain-37185.herokuapp.com/article/query";
    
    AFHTTPSessionManager *findManager = [[AFHTTPSessionManager alloc] init];
    
    [findManager GET:url parameters:dictionary progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"RESPONSE %@", responseObject);
        
        FEMMapping *mapping = [ArticleModel defaultMapping];
        NSArray *models = [FEMDeserializer collectionFromRepresentation:responseObject mapping:mapping];
        succses(models);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        error = [NSError errorWithDomain:@"Downloading failed" code:-1 userInfo:nil];
        failure(error);
    }];
    

    
}

@end
