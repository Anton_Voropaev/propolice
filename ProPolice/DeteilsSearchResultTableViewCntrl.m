//
//  DeteilsSearchResultTableViewCntrl.m
//  ProPolice
//
//  Created by Anton Voropaev on 24.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//


#import "DeteilsSearchResultTableViewCntrl.h"

@interface DeteilsSearchResultTableViewCntrl ()
@property (strong, nonatomic) ArticleModel *articleModel;
@property (strong, nonatomic) NSString *searchText;


@end

@implementation DeteilsSearchResultTableViewCntrl 


- (instancetype)initWithArticleModel:(ArticleModel*)article andSearchText:(NSString *)searchText {
    if (self = [super init]) {
        
        self.articleModel = article;
        self.searchText = searchText;
        [self makingTableViewWithNavBar];
       
        
    }
    return self;
}

- (void)makingTableViewWithNavBar {
    
    UITableView *myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-64) style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    myTableView.estimatedRowHeight = 80;
    myTableView.rowHeight = UITableViewAutomaticDimension;
    myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:myTableView];
    
    UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 64)];
    [self.view addSubview:navBar];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = CGRectMake(10, 0, 50, 80);
    [button setTitle:@"Назад" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [navBar addSubview:button];
}

- (void)buttonPressed {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UItableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.articleModel.parts count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.font = [UIFont fontWithName:@"Courier" size:18];
    
    NSString *part = [self.articleModel.parts objectAtIndex:indexPath.row];
    NSMutableAttributedString *articleText = [[NSMutableAttributedString alloc] initWithString:part];

    if ([part containsString:self.searchText]) {
        
        NSRange searchRange = NSMakeRange(0, part.length);
        while (YES) {
            NSRange range = [part rangeOfString:self.searchText options:0 range:searchRange];
            if (range.location != NSNotFound) {
                
            [articleText addAttribute:NSBackgroundColorAttributeName value:[UIColor yellowColor] range:range];

                NSInteger index = range.location + range.length;
                searchRange.location = index;
                searchRange.length = [part length] - index;
            }else {
                break;
            }
        }

        [cell.textLabel setAttributedText:articleText];
        
    } else {
        
        cell.textLabel.text = part;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegete

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
