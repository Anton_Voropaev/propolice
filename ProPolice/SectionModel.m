//
//  SectionModel.m
//  ProPolice
//
//  Created by Anton Voropaev on 13.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import "SectionModel.h"


@implementation SectionModel

+ (FEMMapping*)defaultMapping {
    
    FEMMapping *mapping = [[FEMMapping alloc] initWithObjectClass:[SectionModel class]];
    [mapping addAttributesFromArray:@[@"name", @"title"]];
    [mapping addAttributesFromDictionary:@{@"sectionID":@"id"}];

    return mapping;
}
@end
