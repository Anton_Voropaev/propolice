//
//  SettingsViewController.m
//  ProPolice
//
//  Created by Anton Voropaev on 15.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import "SettingsViewController.h"

@implementation SettingsViewController

- (void)viewDidLoad {
    self.view.backgroundColor = [UIColor whiteColor];
    
    UINavigationBar *navBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 64)];
    [self.view addSubview:navBar];
    
    UILabel *settingslable = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(navBar.bounds)-60, 15, 120, 44)];
    settingslable.text = @"Settings";
    settingslable.textAlignment = NSTextAlignmentCenter;
    [navBar addSubview:settingslable];
    
    
    UILabel *mainLable = [[UILabel alloc] init];
    mainLable.frame = CGRectMake(CGRectGetMidX(self.view.bounds)-75, CGRectGetMidY(self.view.bounds)-40, 150, 80);
    mainLable.text = @"Налаштування додадтку";
    mainLable.font = [UIFont fontWithName:@"AppleSDGothicNeo-Thin" size:20];
    mainLable.textColor = [UIColor blackColor];
    mainLable.numberOfLines = 0;
    mainLable.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:mainLable];

}



@end
