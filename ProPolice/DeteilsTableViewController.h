//
//  DeteilsTableViewController.h
//  ProPolice
//
//  Created by Anton Voropaev on 13.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleModel.h"

@interface DeteilsTableViewController : UITableViewController


- (instancetype)initWithArticleModel:(ArticleModel*)model;

@end
