//
//  ArticleTableViewController.m
//  ProPolice
//
//  Created by Anton Voropaev on 13.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//
#import "ArticleTableViewController.h"
#import "AVServerManager.h"
#import "ArticleModel.h"
#import "DeteilsTableViewController.h"
#import "Calculation.h"

@interface ArticleTableViewController ()
@property (strong, nonatomic) NSArray *articlesArray;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;
@property (strong, nonatomic) SectionModel *sectionModel;

@end

@implementation ArticleTableViewController


- (instancetype)initWithModel:(SectionModel*)model
{
    self = [super init];
    if (self) {
        
        self.sectionModel = model;
        
        [self.indicator startAnimating];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        NSMutableString *methodName= [[NSMutableString alloc]initWithString:@"article/query?section_id="];
        [methodName appendString:self.sectionModel.sectionID];
        
        
        [[AVServerManager sharedManager]getDataFromServerWithSkip:0 andMethodName:methodName andParams:nil onSuccses:^(NSArray *items) {
            self.articlesArray = [NSArray arrayWithArray:items];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                [self.indicator stopAnimating];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
            
            
        } onFailure:^(NSError *error) {
            
        }];


        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.sectionModel.name;
    
}

- (void)makingIndicator {
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.indicator = indicator;
}




#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.articlesArray count];
    
    
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    ArticleModel *article = [self.articlesArray objectAtIndex:indexPath.row];
    
    NSString *artNum = article.number;
    NSString *artTitle = article.title;
    NSLog(@"%@", artNum);///// почему атрибуты применяются не ко всей строке??????
    
    NSMutableAttributedString *numberString = [[NSMutableAttributedString alloc] initWithString:artNum attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Courier-Bold" size:16]}];
    
    NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:artTitle attributes:@{NSFontAttributeName: [UIFont fontWithName:@"CourierNewPSMT" size:16]}];
    
    [numberString appendAttributedString:titleString];
    [cell.textLabel setAttributedText:numberString];
    cell.textLabel.numberOfLines = 0;
    
 

    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ArticleModel *model = [self.articlesArray objectAtIndex:indexPath.row];
    
    for (NSString *part in model.parts) {
        NSString *string = part;
        NSLog(@"%@", string);
    }
    
    DeteilsTableViewController *dtvc = [[DeteilsTableViewController alloc] initWithArticleModel:model];
    [self.navigationController pushViewController:dtvc animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ArticleModel *article = [self.articlesArray objectAtIndex:indexPath.row];
    NSString *string = [NSString stringWithFormat:@"%@ %@", article.number, article.title];
    UIFont *font = [UIFont fontWithName:@"AlNile-Bold" size:16];
    NSDictionary *dict = @{NSFontAttributeName: font};
    
    return [Calculation heightForText:string withParams:dict];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





@end
