//
//  AVServerManager.h
//  ProPolice
//
//  Created by Anton Voropaev on 11.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AVServerManager : NSObject

+ (AVServerManager *)sharedManager;

- (void)getDataFromServerWithSkip:(NSInteger)skip andMethodName:(NSString*)method andParams:(NSDictionary*)params onSuccses:(void(^)(NSArray*items))succses onFailure:(void(^)(NSError *error))failure;

- (void)findItemsWithParams:(NSDictionary *)dictionary onSuccses:(void(^)(NSArray*items))succses onFailure:(void(^)(NSError *error))failure;

@end