//
//  ArticleModel.m
//  ProPolice
//
//  Created by Anton Voropaev on 13.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import "ArticleModel.h"

@implementation ArticleModel

+ (FEMMapping*)defaultMapping {
    
    FEMMapping *mapping = [[FEMMapping alloc] initWithObjectClass:[ArticleModel class]];
    [mapping addAttributesFromArray:@[@"number", @"title", @"parts"]];
    [mapping addAttributesFromDictionary:@{@"articleID":@"id", @"sectionID":@"section_id"}];
    
    return mapping;
    
}

@end
