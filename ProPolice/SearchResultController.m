//
//  SearchResultController.m
//  ProPolice
//
//  Created by Anton Voropaev on 11.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import "SearchResultController.h"
#import "ArticleModel.h"
#import "DeteilsSearchResultTableViewCntrl.h"


@interface SearchResultController ()

@end

@implementation SearchResultController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.filteredArray = [NSMutableArray array];
    
    self.tableView.contentInset = UIEdgeInsetsMake(64, 0, 64, 0);
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.tableView.estimatedRowHeight = 80;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
}



#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  [self.filteredArray count];
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    
    ArticleModel *article = [self.filteredArray objectAtIndex:indexPath.row];
    
    NSString *artNum = article.number;
    NSString *artTitle = article.title;
    NSLog(@"%@", artNum);
    
    NSMutableAttributedString *numberString = [[NSMutableAttributedString alloc] initWithString:artNum attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Courier-Bold" size:16]}];
    
    NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:artTitle attributes:@{NSFontAttributeName: [UIFont fontWithName:@"CourierNewPSMT" size:16]}];
    
    [numberString appendAttributedString:titleString];
    [cell.textLabel setAttributedText:numberString];
    cell.textLabel.numberOfLines = 0;
    

    return cell;
}

#pragma  mark - UItableViewDelegate 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ArticleModel *model = [self.filteredArray objectAtIndex: indexPath.row];
    DeteilsSearchResultTableViewCntrl *dsrtvc = [[DeteilsSearchResultTableViewCntrl alloc] initWithArticleModel:model andSearchText:self.searhText];
    [self presentViewController:dsrtvc animated:YES completion:^{
            //// network activity indicator?????
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
