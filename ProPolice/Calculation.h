//
//  Calculation.h
//  ProPolice
//
//  Created by Anton Voropaev on 16.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Calculation : NSObject

+ (CGFloat)heightForText:(NSString *)text withParams:(NSDictionary*)attributes;
@end
