//
//  DeteilsSearchResultTableViewCntrl.h
//  ProPolice
//
//  Created by Anton Voropaev on 24.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleModel.h"

@interface DeteilsSearchResultTableViewCntrl : UIViewController <UITableViewDataSource, UITableViewDelegate>

- (instancetype)initWithArticleModel:(ArticleModel*)article andSearchText:(NSString*)searchText;

@end
