//
//  SearchResultController.h
//  ProPolice
//
//  Created by Anton Voropaev on 11.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultController : UITableViewController

@property (strong, nonatomic) NSArray *filteredArray;

@property (strong, nonatomic) NSString *searhText;

@end
