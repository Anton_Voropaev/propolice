//
//  Calculation.m
//  ProPolice
//
//  Created by Anton Voropaev on 16.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import "Calculation.h"

@implementation Calculation

+ (CGFloat)heightForText:(NSString *)text withParams:(NSDictionary*)attributes {
    
    UIFont *font = [UIFont systemFontOfSize:16];
    NSDictionary *dict = @{NSFontAttributeName: font};
    CGRect rect = [text boundingRectWithSize:CGSizeMake(360, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    
    return CGRectGetHeight(rect);
}

@end
