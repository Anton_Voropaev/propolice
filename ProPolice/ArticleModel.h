//
//  ArticleModel.h
//  ProPolice
//
//  Created by Anton Voropaev on 13.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FEMMapping.h>

@interface ArticleModel : NSObject
@property (strong, nonatomic) NSString *number;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *articleID;
@property (strong, nonatomic) NSString *sectionID;

@property (strong, nonatomic) NSArray *parts; //NSString


+ (FEMMapping*)defaultMapping;

@end
