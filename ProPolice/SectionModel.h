//
//  SectionModel.h
//  ProPolice
//
//  Created by Anton Voropaev on 13.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FEMMapping.h>

@interface SectionModel : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *sectionID;

+ (FEMMapping*)defaultMapping;


@end
