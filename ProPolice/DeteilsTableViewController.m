//
//  DeteilsTableViewController.m
//  ProPolice
//
//  Created by Anton Voropaev on 13.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import "DeteilsTableViewController.h"
#import "Calculation.h"


@interface DeteilsTableViewController ()
@property (strong, nonatomic) NSMutableArray *dataForTableview;
@property (strong, nonatomic) ArticleModel *model;

@end

@implementation DeteilsTableViewController

- (instancetype)initWithArticleModel:(ArticleModel*)model
{
    self = [super init];
    if (self) {
        self.model = [[ArticleModel alloc] init];
        self.model = model;
        NSLog(@"%@ %@",self.model.number, self.model.title);
        
        self.dataForTableview = [NSMutableArray array];

        for (NSString *string in model.parts) {
            NSString *articlePart = string;
            [self.dataForTableview addObject:articlePart];
            
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

}



#pragma mark - UITableViewDataSource

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    
    return [NSString stringWithFormat:@"%@ %@", self.model.number, self.model.title];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataForTableview count];
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
     
    NSString *part = [self.dataForTableview objectAtIndex:indexPath.row];
    
    NSAttributedString *atrPart = [[NSAttributedString alloc]initWithString:part attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Courier" size:16]}];
    
    [cell.textLabel setAttributedText:atrPart];
    
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *text = [self.model.parts objectAtIndex:indexPath.row];
    UIFont *font = [UIFont systemFontOfSize:16];
    NSDictionary *dict = @{NSFontAttributeName: font};
    CGRect rect = [text boundingRectWithSize:CGSizeMake(360, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:dict context:nil];
    
    return CGRectGetHeight(rect);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    
}


@end
