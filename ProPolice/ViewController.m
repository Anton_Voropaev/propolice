//
//  ViewController.m
//  ProPolice
//
//  Created by Anton Voropaev on 11.05.16.
//  Copyright © 2016 Anton Voropaev. All rights reserved.
//

#import "ViewController.h"
#import "AVServerManager.h"
#import "SectionModel.h"
#import "ArticleTableViewController.h"
#import <AFNetworking.h>
#import "Calculation.h"
#import "SearchResultController.h"
#import "ArticleModel.h"

#import "Reachability.h"

@interface ViewController () <UISearchBarDelegate>

@property(strong, nonatomic)NSArray *models; /// массив моделей
@property (weak, nonatomic) UIActivityIndicatorView *indicator;
@property (strong, nonatomic) UITabBarController *tab;

@property (strong, nonatomic) UIRefreshControl *refresh;

/////search
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) SearchResultController *searchResultController;

@end

@implementation ViewController

static NSString* const sectionAll = @"section/all";

- (void)viewDidLoad {
    [super viewDidLoad];

    
    self.navigationItem.title = @"ЗУ \"Про нацiональну полiцiю\"";

    ////Actions
    [self makingActivityIndicator];
    [self.indicator startAnimating];
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.tintColor = [UIColor grayColor];
    refresh.attributedTitle = [[NSAttributedString alloc] initWithString:@"Оновлення"];
    [refresh addTarget:self action:@selector(refreshView:) forControlEvents:UIControlEventValueChanged];
    
    self.refresh = refresh;
    
    self.refreshControl = self.refresh;
    
    
    //////////////SerchController
    self.searchResultController = [[SearchResultController alloc] init];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultController];
    self.searchController.searchBar.placeholder = nil;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.searchController.searchBar.delegate = self;
    

}



#pragma mark - Checking Internet Connection

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        
        UIAlertController *alertController =[UIAlertController alertControllerWithTitle:@"Увага!" message:@"З'єднання з інтернетом відсутне" preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                       [self.indicator stopAnimating];
                                       [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else {
        
            [self getItemsfromServer];
       
    }}



#pragma mark - UI

- (void)makingActivityIndicator {
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.indicator = indicator;

}



#pragma mark - Actions


- (void)refreshView:(UIRefreshControl *)refresh  {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [self getItemsfromServer];
        
    });
}


#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    NSString *searchText = searchBar.text;
    NSDictionary *params = @{@"parts":searchText, @"extended":@"parts"};
    [[AVServerManager sharedManager]findItemsWithParams:params onSuccses:^(NSArray *items) {
        
        NSLog(@"SEARCH button clicked array = %@", items);
        for (ArticleModel *article in items) {
            NSLog(@"номера статей = %@", article.number);
        }
        
        self.searchResultController = (SearchResultController*)self.searchController.searchResultsController;
        
//        NSUserDefaults *userDef = [NSUserDefaults standardUserDefaults];
//        [userDef setObject:items forKey:@"filteredArray"];
//        [userDef setObject:searchBar.text forKey:@"searchText"];
//        [userDef synchronize];
//        
//        NSNotification *notif = [NSNotification notificationWithName:@"testNotification" object:nil];
//        [[NSNotificationCenter defaultCenter]
//         postNotification:notif];
        
        
        
        self.searchResultController.filteredArray = items;
        self.searchResultController.searhText = searchBar.text;
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.searchResultController.tableView reloadData];
        });

        
        
    } onFailure:^(NSError *error) {
        
    }];
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    
    NSLog(@"CANCEL BUTTON CLICKED!");

}


#pragma mark - API Pro'Police

- (void)getItemsfromServer {
    
        
        [[AVServerManager sharedManager]getDataFromServerWithSkip:0 andMethodName:sectionAll andParams:nil onSuccses:^(NSArray *items) {
            
            self.models = [NSArray arrayWithArray:items];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.refreshControl endRefreshing];
                
                [self.tableView reloadData];
                
                [self.indicator stopAnimating];
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
            
        } onFailure:^(NSError *error) {
            
            NSLog(@"%@", [error localizedDescription]); ///alertView
            
        }];
    

}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.models count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    SectionModel *model = [self.models objectAtIndex:indexPath.row];
    NSLog(@"%@", model.name);
    
    
    NSMutableAttributedString *nameString = [[NSMutableAttributedString alloc] initWithString:model.name attributes:@{NSFontAttributeName:[UIFont fontWithName:@"AlNile-Bold" size:16]}];
    
    NSAttributedString *titleString = [[NSAttributedString alloc] initWithString:model.title attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16]}];
    
    [nameString appendAttributedString:titleString];
    
    [cell.textLabel setAttributedText:nameString];
    cell.textLabel.numberOfLines = 0;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SectionModel *model = [self.models objectAtIndex:indexPath.row];
    
    ArticleTableViewController *atvc = [[ArticleTableViewController alloc] initWithModel:model];
    [self.navigationController pushViewController:atvc animated:YES];
    
}


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SectionModel *model = [self.models objectAtIndex:indexPath.row];
    NSString *string = [NSString stringWithFormat:@"%@ %@", model.name, model.title];
    NSDictionary *dict = @{NSFontAttributeName: [UIFont fontWithName:@"AlNile-Bold" size:16]};
    return [Calculation heightForText:string withParams:dict];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}













@end